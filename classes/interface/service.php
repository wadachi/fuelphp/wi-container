<?php
/**
 * Wadachi FuelPHP Dependency Container Package
 *
 * An Implementation of the FuelPHP dependency container.
 * https://github.com/fuelphp/dependency
 *
 * @package    wi-container
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * サービス ベース
 */
interface Interface_Service
{
}
