<?php
/**
 * Wadachi FuelPHP Dependency Container Package
 *
 * An Implementation of the FuelPHP dependency container.
 * https://github.com/fuelphp/dependency
 *
 * @package    wi-container
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * 依存性注入コンテナ
 */
final class Container
{
  // 依存性注入コンテナ
  private static $container = null;

  /**
   * 静的コンストラクタ
   * 依存性注入コンテナの設定
   *
   * @access public
   */
  public static function _init()
  {
    \Config::load('di', true);
    static::$container = new \Fuel\Dependency\Container(['di' => \Config::get('di')]);
  }

  /**
   * コンテナ オブジェクトを取得
   *
   * @access public
   * @param string $key オブジェクト キー
   * @return オブジェクト
   */
  public static function get($key)
  {
    return static::$container->get($key);
  }
}
