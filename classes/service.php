<?php
/**
 * Wadachi FuelPHP Dependency Container Package
 *
 * An Implementation of the FuelPHP dependency container.
 * https://github.com/fuelphp/dependency
 *
 * @package    wi-container
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * サービス ベース
 */
class Service extends \League\Container\ServiceProvider implements Interface_Service
{
  /**
   * 一定の値を取得する
   *
   * @access private
   * @param string $name 一定名
   * @return mixed 一定の値
   */
  private function get_constant($name)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $reflector = new ReflectionClass(get_class($this));
    return $reflector->getConstant($name);
  }// </editor-fold>

  /**
   * コンテナでサービスを登録する
   *
   * @access public
   */
  public function register()// <editor-fold defaultstate="collapsed" desc="...">
  {
    $provides = $this->get_constant('_PROVIDES');
    $depends = $this->get_constant('_DEPENDS');
    $depends = is_array($depends) ? $depends : [];

    if (is_string($provides))
    {
      $provides = [$provides];
    }

    static::$provides = $provides;

    foreach ($provides as $service)
    {
      $registration = $this->container->add($service, get_class($this));

      foreach ($depends as $key => $dependency)
      {
        $registration->withMethodCall($key, [$dependency]);
      }
    }
  }// </editor-fold>
}
