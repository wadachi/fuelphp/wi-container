<?php
/**
 * Wadachi FuelPHP Dependency Container Package
 *
 * An Implementation of the FuelPHP dependency container.
 * https://github.com/fuelphp/dependency
 *
 * @package    wi-container
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

\Autoloader::add_classes([
  'Wi\\Container' => __DIR__.'/classes/container.php',
  'Wi\\Interface_Service' => __DIR__.'/classes/interface/service.php',
  'Wi\\Service' => __DIR__.'/classes/service.php',
]);
