<?php
/**
 * Wadachi FuelPHP Dependency Container Package
 *
 * An Implementation of the FuelPHP dependency container.
 * https://github.com/fuelphp/dependency
 *
 * @package    wi-container
 * @version    0.1
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * 依存性注入コンテナの設定
 */
return [
/* こんな形
  'service.mail' => [
    'class' => 'Service_Mail',
  ],

  'service.login' => [
    'class' => 'Service_Login',
    'methods' => [
      'set_mail_service' => ['service.mail'],
    ]
  ],
*/
];
